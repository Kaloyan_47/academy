import './App.css';

function FirstPart() {
  return (
    <div>
      <nav></nav>
      <br></br>
    <header class="first-part" >
        <img src={require("./imiges/pilow-top-diff-angle.jpg")} class="half-pictures" alt="" />
        <div class="first-p-align">
            <img src={require("./imiges/Spine_align_logo.png")} id="logo" alt="" />
            <p class="first-p">All our pillows are made of the best materials, ensuring
                long-lasting durability and a unique feeling of softness.
                Whether you prefer velvet, silk thread or cotton, our cushons offer
                the perfect combination of luxury and practicality.
            </p>
            <div class="button-alignment">
                <button href="#" class="left-button"></button>
                <button href="#" class="right-button"><strong>BUY NOW</strong></button>
            </div>
        </div>
    <header />

  </header>
     <div class="info-container">
     <h1 class="info-h">LUX PILLOW</h1>
     <div class="info-items">
         <div class="info-first-div">

             <div class="info-top-left">
                 <div class="counter-left">
                     <h1 id="info-counter" class="left-align">01</h1>
                 </div>
                 <h2 class="text-center" id="color-change">Design and Style</h2>
                 <p class="text-right-align">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint dolorem
                     provident aut consequuntur omnis unde repudiandae quam, itaque dolor vitae fuga esse
                     iusto aliquam rem, voluptas inventore consectetur asperiores dicta?
                 </p>
             </div>
             <div class="info-top-right">
                 <div>
                     <h1 id="info-counter" class="left-align">03</h1>
                 </div>
                 <h2 class="text-center" id="color-change">Strong customer satisfaction</h2>
                 <p class="text-right-align">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint dolorem
                     provident aut consequuntur omnis unde repudiandae quam, itaque dolor vitae fuga esse
                     iusto aliquam rem, voluptas inventore consectetur asperiores dicta?
                 </p>
             </div>
         </div>
         <img src={require("./imiges/pilow-straight.jpg")} class="info-img" alt="" />
         <div class="info-second-div">
             <div class="info-bottom-left">
                 <h1 class="text-right-align" id="info-counter-right">02</h1>
                 <h2 class="text-center" id="color-change">Unbeateble comfort</h2>
                 <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint dolorem
                     provident aut consequuntur omnis unde repudiandae quam, itaque dolor vitae fuga esse
                     iusto aliquam rem, voluptas inventore consectetur asperiores dicta?
                 </p>
             </div>
             <div class="info-bottom-right">
                 <h1 class="text-right-align" id="info-counter-right">04</h1>
                 <h2 class="text-center" id="color-change">Easy ordering and fast delivery</h2>
                 <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint dolorem
                     provident aut consequuntur omnis unde repudiandae quam, itaque dolor vitae fuga esse
                     iusto aliquam rem, voluptas inventore consectetur asperiores dicta?
                 </p>
             </div>
         </div>
     </div>
 </div>

 <div class="lorem-div">
        <p class="lorem" id="lorem-eins">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut voluptatibus rem
            at.
            Iste corrupti laborum fugit magnam quis aliquid amet! Qui
            doloremque, numquam eligendi odit ipsa nam dolorum temporibus beatae.
        </p>
        <p class="lorem">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut voluptatibus rem at.
            Iste corrupti laborum fugit magnam quis aliquid amet! Qui
            doloremque, numquam eligendi odit ipsa nam dolorum temporibus beatae.
        </p>
        <p class="lorem" id="lorem-drei">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut voluptatibus rem
            at.
            Iste corrupti laborum fugit magnam quis aliquid amet! Qui
            doloremque, numquam eligendi odit ipsa nam dolorum temporibus beatae.
        </p>

    </div>

    <div class="two-img-two-p">
        <p class="half-split-p">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus
            eum id amet dolorem quia sit ipsa asperiores esse nulla, corrupti optio
            impedit eos ipsum, at nesciunt! Vero blanditiis esse reiciendis.
        </p>
        <img src={require("./imiges/multiple-pilows.jpg")} class="half-split" alt="" />
        <img src={require("./imiges/squished-pilow.jpg")} class="half-split" alt="" />
        <p class="half-split-p">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus
            eum id amet dolorem quia sit ipsa asperiores esse nulla, corrupti optio
            impedit eos ipsum, at nesciunt! Vero blanditiis esse reiciendis.
        </p>

    </div>

    <div class="first-picture-bg">
        <img src={require("./imiges/face-shot-piwol.jpg")} id="bg-picture" alt="" />
        <h3 class="text-center" id="bg-text">Experience Comfort and Elegance</h3>
        <p class="text-center" id="bg-text">At Spine Align, we believe that every detail of your home should exude
            style and comfort. Our luxury pillow collection has been created with those who
            value sophisticated design and unmatched comfort in mind.
        </p>
        <button href="#" class="button-center">BUY NOW</button>

    </div>

    <div class="second-img-bg">
        <img src={require("./imiges/side-body-shot-pilow.jpg")} class="bg-img" alt="" />
        <h3 class="text-center" id="front-text">Welcome to the world of refined comfort</h3>
        <p class="text-center" id="second-imige-text">Thret yourself to the luxury you deserve!</p>
    </div>

    <div class="last-part">
        <img src={require("./imiges/pilow-top.jpg")} class="half-pictures" alt="" />
        <div class="last-p">
            <h1 id="text-color-change" class="text-center">LUX PILLOW</h1>
            <p class="text-center">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Est iure
                illum provident, quis ipsam unde illo eveniet velit culpa aspernatur atque
                dolores eligendi hic incidunt repellendus dicta at sint recusandae?
            </p>
            <button href="#" class="last-button">BUY NOW</button>
        </div>

    </div>

    <img src={require("./imiges/exam_bottom.png")} class="logo_bottom" alt="" ></img>
 </div>);
}



export default function MyApp() {
  return (
    <FirstPart />
 
  );
}
