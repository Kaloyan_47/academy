let pens = 4
let markers = 2
let liters_cleaning = 5
let discount_percent = 13

let discount = discount_percent / 100
let price_pen = pens * 5.80;
let price_markers = markers * 7.20;
let price_cleaning = liters_cleaning * 1.20;
let all_price = price_cleaning + price_pen + price_markers
let total_sum = all_price - (all_price * discount)

console.log(total_sum)