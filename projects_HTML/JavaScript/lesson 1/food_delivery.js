let chicken_menu = 9
let fish_menu = 2
let vegan_menu = 6

let chicken_menu_sum = chicken_menu * 10.35
let fish_menu_sum = fish_menu * 12.40
let vegan_menu_sum = vegan_menu * 8.15

let menu_total_sum = chicken_menu_sum + fish_menu_sum + vegan_menu_sum
let desert_price = menu_total_sum * 0.20

let total_sum = menu_total_sum + desert_price + 2.50
console.log(total_sum)